import React from 'react'
import ImageShow from '../components/content/product/ImageShow';
import EditProduct from '../components/content/product/EditProduct';
import ImageModal from '../components/content/product/ImageModal';
export const columns = [
    {
        columns: [
            {
                Header: "Sản phẩm",
                accessor: "title"
            }
        ]
    }, {

        columns: [
            {
                Header: "Mô tả",
                accessor: "desciption"
            }, {
                Header: "Giá",
                accessor: "price"
            }
        ]
    }, {

        columns: [
            {
                Header: "Số lượng",
                accessor: "quantity"
            }
        ]
    }, {

        columns: [
            {
                Header: "Khuyến mãi",
                accessor: "discount"
            }
        ]
    }, {

        columns: [
            {
                Header: "Trạng thái",
                Cell: row => (row.original.status === "selling")
                    ? "Đang bán"
                    : "Đang chờ"
            }
        ]
    }, 
    {

        columns: [
            {
                Header: "Hình ảnh",
                Cell: row =>{
                   return  (<ImageModal id={row.original._id}/>)
                } 
            }

        ]
    },
    {

        columns: [
            {
                Header: "Cập nhật",
                Cell: <div>
                        <EditProduct/>
                        &nbsp;&nbsp;&nbsp;&nbsp;

                    </div>

            }
        ]
    }
];
export default columns;