import React, {Component} from 'react'
import apiCaller from '../../../utils/apiCaller'
import AddProduct from './AddProduct';
import swal from 'sweetalert';
import $ from 'jquery';
import './style.css'
import ReactPaginate from 'react-paginate';
import ReactTable from 'react-table'
import "react-table/react-table.css";
import UploadFile from './UploadFile'
import EditProduct from './EditProduct';
import HeadingContent from '../HeadingContent';


export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            docs: [],
            currentPage: 1,
            totalDocs: 0,
            limit: 10,
            isload: false,
            images: [],
            id: ""
        }
    }
    componentDidMount() {
        
      apiCaller("products/list/manage?page=1", "GET").then(res => {
            if (!res) {
                swal("THẤT BẠI", "Không thể load dữ liệu", "danger")
            } else {
                this.setState({totalDocs: res.data.totalDocs, images: res.data.docs.images, docs: res.data.docs})
            }

        })

    }
    //Start ---Update new API
    updateAPI = async() => {
        var currentPage = this.state.currentPage
        await apiCaller("products/list/manage?page=" + currentPage, "GET", null).then(res => {
            console.log(res)
            this.setState({docs: res.data.docs})
        })
    }
    //End -- Update new API

//Start - Process Image
    deleteImage = (image, product, index) => {
        if (image && product) {
            console.log("Delete Image");
            var images = {
                images: []
            }
            var new_images = this.state.images
            images.images.push(image)
            apiCaller("products/" + product + "/images?", "DELETE", images).then(res => {
                swal("Thành công", "Bạn đã xóa hình sản phẩm", "success");
                new_images.splice(index, 1);
                this.setState({images: new_images})
            })
        }

    }
    
    updateImage = (id) => {
        this.state.images.push(id);
        console.log(this.state.images)
        this.setState({isload: true})
    }

    //END -- Image
    //Start -- Toggle Active
    active_click = (id) => {
        swal({text: "Bạn có muốn kích hoạt sản phẩm này không?", icon: "warning", buttons: true, dangerMode: true}).then((willDelete) => {
            if (willDelete) {
                apiCaller("products/active/" + id, "PUT", null).then(res => {
                    console.log(res);
                    this.updateAPI();
                    swal("Bạn đã kích hoạt thành công!", {icon: "success"});
                })

            } else {}
        });
    }
    deactive_click = (id) => {
        swal({text: "Bạn có muốn vô hiệu sản phẩm này không?", icon: "warning", buttons: true, dangerMode: true}).then((willDelete) => {
            if (willDelete) {
                apiCaller("products/deactive/" + id, "PUT", null).then(res => {
                    console.log(res);

                    this.updateAPI();
                    swal("Bạn đã vô hiệu thành công!", {icon: "success"});
                })

            } else {}
        });
    }
    //End-- Toggle Active

    //Start --Detail Info Image of Products
    onImageClick = async(_id) => {
        var id = _id;
        const data = await apiCaller("products/details/" + id, "GET", null);
        this.setState({
            images: data.data.images,
            isload: true,
            id: id
        }, () => $('#imageModal').modal("show"))
    }
    //End --Detail Info Image of Products

    //Start -- Change Page
    handlePageClick = (id) => {

        var _id = id.selected + 1
        apiCaller("products/list/manage?page=" + _id, "GET").then(res => {
            this.setState({docs: res.data.docs, currentPage: _id})
        })


    }
    //End -- Chang Page

    render() {
        const columns = [
            {
                columns: [
                    {
                        Header: "Sản phẩm",
                        accessor: "title"
                    }
                ]
            }, {

                columns: [
                    {
                        Header: "Mô tả",
                        accessor: "desciption"
                    }, {
                        Header: "Giá",
                        accessor: "price"
                    }
                ]
            }, {

                columns: [
                    {
                        Header: "Số lượng",
                        accessor: "quantity"
                    }
                ]
            }, {

                columns: [
                    {
                        Header: "Khuyến mãi",
                        accessor: "discount"
                    }
                ]
            }, {

                columns: [
                    {
                        Header: "Trạng thái",

                        Cell: row => (row.original.status === "selling")
                            ? <span className="badge badge-light ">Đang bán</span>
                            : <span className="badge badge-dark ">Đang chờ</span>
                    }
                ]
            }, {

                columns: [
                    {
                        Header: "Hình ảnh",
                        accessor: "images",
                        Cell: row => (
                            <span
                                onClick={() => this.onImageClick(row.original._id)}
                                className="badge badge-info">Xem chi tiết</span>
                        )
                    }

                ]
            }, {

                columns: [
                    {
                        Header: "Cập nhật",
                        Cell: row => (row.original.is_active

                            ? <span
                                    onClick=
                                    {()=>this.deactive_click(row.original.id)}
                                    className="badge badge-success">Đang hoạt động

                                </span>

                            : <span
                                onClick=
                                {()=>this.active_click(row.original.id)}
                                className="badge badge-danger">Ẩn</span>)

                    }
                ]
            }, {

                columns: [
                    {
                        width: 30,
                        Cell: row => <EditProduct updateAPI={this.updateAPI} id={row.original._id}/>

                    }
                ]
            }
        ];
        var {docs, isload, images} = this.state
        var {totalDocs, limit} = this.state
        var pagecount = Math.ceil(totalDocs / limit)
        if (images) {
            var image = images.map((img, index) => {
                return (
                    <div className="col-sm-4">

                        <img alt={true} key={index} width="150px" src={img}/>
                        <button onClick={() => this.deleteImage(img, this.state.id, index)}>
                            x
                        </button>
                        <hr/>
                    </div>

                )
            })
        }

        
        return (



            <div className="container-fluid">
            <HeadingContent title="QUẢN LÝ SẢN PHẨM"/>
            <div style={{paddingBottom:"10px"}}>
                <AddProduct updateAPI={this.updateAPI}/>
            </div>
            
                <div className="row wow fadeIn">
                    <div className="col-md-12 mb-4">
                        <div className="card">
                            <div className="card-body">
                            <div className="row">
                            
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div>
                                        <ReactTable
                                            data={docs}
                                            columns={columns}
                                            defaultPageSize={10}
                                            showPagination={false}
                                            className="-striped -highlight"/>
    
                                    </div>
                                    
                                </div>
    
                            </div>
                            <div className="modal fade" id="imageModal">
                                <div className="modal-dialog modal-lg" role="document">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h4 className="modal-title w-100" id="myModalLabel">Danh mục hình ảnh -
                                            </h4>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div className="modal-body">
    
                                            {isload
                                                ? <div className="row">
                                                        {image}
                                                    </div>
                                                : <div>
                                                    <div className="spinner-grow text-primary" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-secondary" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-success" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-danger" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-warning" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-info" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-light" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-dark" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                </div>
    }
                                            <UploadFile updateImage={this.updateImage} id={this.state.id}/>
    
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <div
                                style={{
                                align: 'center'
                            }}>
    
                                <ReactPaginate
                                    previousLabel={'Trở về'}
                                    nextLabel={'Tiếp theo'}
                                    pageCount={pagecount}
                                    marginPagesDisplayed={2}
                                    pageRangeDisplayed={5}
                                    onPageChange={(e) => this.handlePageClick(e)}
                                    containerClassName={'pagination1'}
                                    pageClassName={'page-item'}
                                    pageLinkClassName={'page-link'}
                                    previousLinkClassName={'page-link'}
                                    nextLinkClassName={'page-link'}
                                    activeClassName={"page-item active"}
                                    disabledClassName ={'disabled'}
                                   
                                    />
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
