import React, {Component} from 'react'
import Sidebar from './header/Sidebar'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import routes from '../routes'


export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin:false
        }
    }
    componentDidMount() {
        this.setState({
            isLogin:true
        })
    }
    
    showContents = (routes) => {
        var result;
        if (routes.length > 0) {
            result = routes.map((route, index) => {
                return (<Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    component={route.main}/>)
            })
           
        }
        return result;
    }
    render() {
        return (
            <Router>
                <div>
                    <header>
                        {/* <Header/> */}
                        <Sidebar/>
                    </header>
                    <main className="pt-5">

                        <Switch>
                            {this.showContents(routes)}
                        </Switch>

                    </main>
                </div>
            </Router>
        )
    }
}
