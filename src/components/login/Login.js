import React, {Component} from 'react'
import './Login.css'
import {NavLink} from 'react-router-dom'
import apiCaller from '../../utils/apiCaller'
import swal from 'sweetalert';
import $ from 'jquery'
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            isLogin:false
        }
    }

    onChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({[name]: value})
    }

    onSubmit = async (event) => {
        event.preventDefault();
        await apiCaller("admin/login", "POST", this.state).then(res => {
            if (!res) {
                swal({
                    title: "Đăng nhập thất bại",
                    icon: "warning",
                  })
            } else {
                localStorage.setItem("user", JSON.stringify({token: res.data.token}))
                this.setState({isLogin: true})
                swal({
                    title: "Đăng nhập thành công",
                    icon: "success",
                  })
                  $("#myModalLabel").modal("hide");
                  this.props.checkLogin(true);
            }

        })

    }
    render() {
       

        // console.log("render")
        // var login = localStorage.getItem("user");
        // if(login !== null) {
        //     return <Router><Redirect to='/homepage'/></Router>
        // } 
        return (
            <div>
                {/* Modal */}
                <div className="form">
                    <div className="modal-dialog" role="document">
                        {/*Content*/}
                        <div className="modal-content form-elegant">
                            {/*Header*/}
                            <div className="modal-header text-center">
                                <h3
                                    className="modal-title w-100 dark-grey-text font-weight-bold mb-1"
                                    id="myModalLabel">
                                    <strong>ĐĂNG NHẬP</strong>
                                    <NavLink to='/notfound' ></NavLink>
                                </h3>

                            </div>
                            {/*Body*/}
                            <div className="modal-body mx-4">
                                {/*Body*/}
                                <form onSubmit={this.onSubmit}>
                                    <div className="md-form md-3">
                                        <input
                                            value={this.state.email}
                                            onChange={this.onChange}
                                            name="email"
                                            type="text"
                                            className="form-control"
                                            placeholder="Email"/> {/* <small className="text-danger mt-10">{this.state.formError.txtEmail}</small> */}
                                    </div>

                                    <div className="md-form md-3">
                                        <input
                                            value={this.state.password}
                                            onChange={this.onChange}
                                            name="password"
                                            type="password"
                                            className="form-control"
                                            placeholder="Mật khẩu"/> {/* <small className="text-danger mt-10">{this.state.formError.txtPassword}</small>                                   */}
                                    </div>

                                    <div className="text-center mb-3">
                                        <button
                                            type="submit"
                                            className="btn blue-gradient btn-block btn-rounded z-depth-1a">Đăng nhập</button>
                                    </div>
                                </form>

                            </div>
                            {/*Footer*/}

                        </div>
                        {/*/.Content*/}
                    </div>
                </div>                
            </div>

        )
    }
}
