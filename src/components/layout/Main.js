import React, { Component } from 'react';
import bg1 from '../layout/bg1.png';
import Login from '../login/Login';
class Main extends Component {
    onClick = ()=> {
        return(
            <Login/>
        )
        
    }
    render() {
        return (
    <div>
        <div className="w3-bar w3-black w3-hide-small">
          <a href="#" className="w3-bar-item w3-button w3-right" onClick={this.onClick}>
              <i className="far fa-user" />
          </a>
        </div>
        <div className="w3-content" style={{maxWidth: '1600px'}}>
          <header className="w3-container w3-center w3-padding-48 w3-white">
            <h1 className="w3-xxxlarge"><b>LINHKIENDIENTU</b></h1>
            <h6>Chào mừng bạn đến với <span className="w3-tag">LINHKIENDIENTU</span></h6>
          </header>
          <header className="w3-display-container w3-wide" id="home">
            <img className="w3-image" src={bg1} alt="LINHKIENDIENTU" width={1600} height={1060} />
          </header>
        </div>
    </div>
      

        );
    }
}

export default Main;