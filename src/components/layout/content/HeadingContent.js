import React, {Component} from 'react'
import ModalAddCategory from '../../categoryProducts/ModaAddCategory';
export default class HeadingContent extends Component {
    render() {
        return (
            <div className="card mb-4 wow fadeIn">
                {/*Card content*/}
                <div className="card-body d-sm-flex justify-content-between">
                    <h4 className="mb-2 mb-sm-0 pt-1">
                        <a href="https://mdbootstrap.com/docs/jquery/" target="_blank">TRANG CHỦ</a>
                        <span>&nbsp;/&nbsp;</span>
                        <span>{this.props.title}</span>
                    </h4>
                    <form className="d-flex justify-content-center">
                        {/* Default input */}
                        <input
                            type="search"
                            placeholder="Type your query"
                            aria-label="Search"
                            className="form-control"/>
                        <button className="btn btn-primary btn-sm my-0 p" type="submit">
                            <i className="fas fa-search"/>
                        </button>
                    </form>
                </div>
            </div>

        )
    }
}
