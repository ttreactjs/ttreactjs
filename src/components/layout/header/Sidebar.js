import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'

export default class Sidebar extends Component {
    render() {
        return (
            <div className="sidebar-fixed position-fixed">
                <a className="logo-wrapper waves-effect">
                    <img
                        src="https://mdbootstrap.com/img/logo/mdb-email.png"
                        className="img-fluid"
                        alt="true"/>
                </a>
                <div className="list-group list-group-flush">
                    <NavLink to="http://www.google.com.vn" className="list-group-item waves-effect">
                        <i className="fas fa-chart-pie mr-3"/>Trang chủ
                    </NavLink>
                    <NavLink
                        to="/sanpham"
                        className="list-group-item list-group-item-action waves-effect">
                        <i className="fas fa-user mr-3"/>Sản phẩm</NavLink>
                    <NavLink
                        to="/loaisanpham"
                        className="list-group-item list-group-item-action waves-effect">
                        <i className="fas fa-table mr-3"/>Loại sản phẩm</NavLink>
                    <NavLink
                        to="/donhang"
                        className="list-group-item list-group-item-action waves-effect">
                        <i className="fas fa-table mr-3"/>Đơn hàng</NavLink>
                    <NavLink
                        to="/khachhang"
                        className="list-group-item list-group-item-action waves-effect">
                        <i className="fas fa-table mr-3"/>Khách hàng</NavLink>

                </div>
            </div>

        )
    }
}
