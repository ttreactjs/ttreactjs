import React, {Component} from 'react'

export default class Header extends Component {
    onLogout () {
        localStorage.clear();
        window.location.href = '/';
    }
    render() {
        return (

            <nav
                className="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
                <div className="container-fluid">
                
                    <a
                        className="navbar-brand waves-effect"
                        href="https://mdbootstrap.com/docs/jquery/"
                    >
                        <strong className="blue-text">MDB</strong>
                    </a>
                    <button type="button" className="btn blue-gradient" onClick={this.onLogout}>Đăng Xuất</button>
                </div>
            </nav>

        )
    }
}
