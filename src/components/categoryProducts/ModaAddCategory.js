import React, {Component} from 'react'
import apiCaller from '../../utils/apiCaller';
import $ from 'jquery'
import swal from 'sweetalert';

export default class ModalAddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            category: '',
            subs: [],
            name: '',
            item:''
        }
    }

    onChange = (e) =>{
        var target = e.target;
        var nam = target.name;
        var val = target.value;	
        this.setState({
            [nam] : val,
        })
    }
    onSubmit = (e) =>{
        e.preventDefault()
        e.target.reset()
        var {name} = this.state
        var text = {name};
        this.setState({
            name: '',
            subs: [...this.state.subs, text] 
        })
    }

    onSave = () =>{
        var {category, subs} = this.state
        apiCaller("categories/create/", "POST", {
            name: category,
            sub_category: subs
        }).then((res) => {
            swal("", "Thêm thành công!", "success");
            $("#ModalAddSub").modal("hide");
            console.log("Create",res)
        })
    }
    shownewsub = (subs) =>{
    var result = {};
    result = subs.map((sub, index) =>{
        return (
            <li key={index} className="list-group-item left">
                 
                <button type="button" className="fas fa-times text-right del" onClick={()=>this.onDelSub(index)} style={{alignContent:"right"}}>
                {/* <div>
           {
              this.props.data.map(el=>
                  <p onClick={this.delete.bind(this, el)}>{el}</p>
              )
           }
         </div> */}
                </button>
                {sub.name}
                {/* <span className="badge" > x </span> */}
            </li>
            
        )
    })
    return result;
}
onDelSub = (index) => {
    var new_subs=[]
    var new_subs = this.state.subs;
    console.log("new_sub_state",new_subs);
    new_subs.splice(index,1)

    console.log("slice",new_subs);
    this.setState({
        subs:new_subs
    },()=>console.log(this.state.subs))
}

render() {
    var {subs} =this.state
    return (         
        <div>
             {/* Button trigger modal */}
             <button type="button" className="btn btn-primary mb-10 " data-toggle="modal" data-target="#exampleModal"><i className="far fa-plus-square"></i></button>
                    {/* Modal */}
                    <div className="modal fade" id="exampleModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                    <div className="modal-content">

                        {/* Header */}
                        <div className="modal-header">
                        <h5 className="modal-title">Thêm Loại Sản Phẩm</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>

                        {/* Body */}
                        <div className="modal-body">
                        <div className="form-group">
                            <label className="col-form-label">Tên Loại:</label><br/>
                            <input type="text" name="category" className="form-control" onChange={this.onChange}/>
                        </div>
                        <form onSubmit={this.onSubmit}> 
                        <div className="form-group">
                            <label className="col-form-label">Tên Phân Loại:</label><br/>
                            <input type="text" name="name" className="form-control" onChange={this.onChange}/>
                        </div>
                        </form>
                        </div>
                        <ul className="list-group">
                            {this.shownewsub(subs)}
                        </ul>
                        
                        {/* Footer */}
                        <div className="modal-footer">
                        <button type="button" className="btn btn-danger" data-dismiss="modal"><i className="far fa-window-close"></i></button>
                        <button type="button" className="btn btn-info" onClick={this.onSave}><i className="fas fa-save"></i></button>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    )
}
}
//     handleAdd = (_id) => {
        
//         var header = {
//             "Authorization": "Bearer " + JSON
//                 .parse(localStorage.getItem('user'))
//                 .token
//         };
//         apiCaller("categories/create/" + _id,'POST',this.state,header).then(res=>{
//             swal("","Cập nhật thành công!", "success");
//             console.log(res)
//         })
//     }
//     onClose = () => {this.setState
//         ({}, () => $('#modalAddCategory').modal("hide"))

//     }
//     onChange = (e) =>{
//         var target = e.target;
//         var nam = target.name;
//         var val = target.value;	
//         this.setState({
//             [nam] : val,
//         })
//     }
//     onSubmit = async(event) => 
//         {
//             event.preventDefault();
//             var header = {
//                 "Authorization": "Bearer " + JSON
//                     .parse(localStorage.getItem('user'))
//                     .token
//             };
//             await apiCaller("categories/create?",'POST',this.state,header).then(res => {
//                 // swal("","Cập nhật thành công!", "success");
//                 this.setState
//             ({
//                 category: 
//                     {
//                         name : res.data.name,
//                         id : res.data._id,
//                         sub_category : res.data.sub_category
//                     }
//             })
//                 swal("","Cập nhật thành công!", "success");

//             })
//         }
//         onSubmitSub = (e) =>{
//             e.preventDefault()
//             e.target.reset()
//             var {name} = this.state
//             var text = {name};
//             this.setState({
//                 name: '',
//                 subs: [...this.state.subs, text] 
//             })
//         }
        
//     render() {
//         var {name,sub,id} = this.state;
        
//         return (
//             <div>

//                 <div className="modal fade" id="modalAddCategory">
//                     <div className="modal-dialog" role="document">
//                         <div className="modal-content">
//                             <div className="modal-header text-center">
//                                 <h4 className="modal-title w-100 font-weight-bold">THÊM LOẠI SẢN PHẨM</h4>
//                                 <button type="button" className="close" data-dismiss="modal" aria-label="Close">
//                                     <span aria-hidden="true">×</span>
//                                 </button>
//                             </div>

//                             <form onSubmit={this.onSubmit}>

//                                 <div className="form-group row">
//                                     <div className='col-sm-1'></div>
//                                     <label
//                                         style={{
//                                         fontWeight: 'lighter'
//                                     }}
//                                         className="col-sm-2 col-form-label">Tên loại</label>
//                                     <div className="col-sm-9">
//                                         <div className="md-form mt-0">
//                                             <input 
//                                              name="name"
//                                              value={name}
//                                              onChange={this.onChange}
//                                              type="text" 
//                                              className="form-control"/>
//                                         </div>
//                                     </div>
//                                 </div>
//                                 <form onSubmit={this.onSubmitSub}> 
//                                 <div className="form-group row">
//                                     <div className='col-sm-1'></div>
//                                     <label
//                                         style={{
//                                         fontWeight: 'lighter'
//                                     }}
//                                         className="col-sm-2 col-form-label">Phân loại</label>
//                                     <div className="col-sm-9">
//                                         <div className="md-form mt-0">
//                                             <input 
//                                              name="sub"
//                                              value={sub}
//                                              onChange={this.onChange}
//                                              type="text" 
//                                              className="form-control"/>
//                                         </div>
//                                     </div>
//                                 </div>
//                              </form>
                                
//                                 {/* <div className="modal-footer d-flex justify-content-center">
//                                 <button type="submit" className="btn btn-indigo" onClick={this.onClose}>
//                                     Xác nhận
//                                     <i className="fas fa-paper-plane-o"/>
//                                 </button>
//                                 </div> */}
//                                 <div className="text-center">
//                                     <button class="btn btn-primary btn-rounded blue-gradient"><i className="fas fa-plus"
//                                     onClick={()=> this.handleAdd(this.state._id)}
//                                     /></button>
//                                     <button class="btn btn-primary btn-rounded blue-gradient"><i className="fas fa-edit"/></button>
//                                     <button class="btn btn-primary btn-rounded blue-gradient"><i className="fas fa-trash"/></button>
//                                 </div>
//                             </form>
                                
                            


                            
//                         </div>
//                     </div>
//                 </div>
//                 <button
//                     data-toggle="modal"
//                     data-target="#modalAddCategory"
//                     // type="submit"
//                     className="fas fa-plus"
//                     >
//                 </button>
                    
//             </div>
//         )
//     }
// }
