import React, {Component} from 'react'
import apiCaller from '../../utils/apiCaller';
import $ from 'jquery'
import swal from 'sweetalert';

export default class ModalAddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subs: 
                {name:'',
                id:''}
            ,
            _id:''    

        }
    }
editOnClick = () => {
    
}
    onChange = (e) =>{
        var target = e.target;
        var name = target.name;
        var val = target.value;	
        this.setState({
            subs:{
                [name] : val
            },
            
        },()=>{console.log(this.state)})
    }
    onSubmit = (e) =>{
        e.preventDefault()
        e.target.reset()
        var {name} = this.state
        var text = {name};
        this.setState({
            name: '',
            subs: [...this.state.subs, text] 
        })
    }

    onSave = (event) =>{
        var { subs} = this.state
        var id = this.props.id_cate;
        console.log(this.props);
        var a = {name:subs.name};
        apiCaller("categories/create/"+id +"/sub_category", "POST", subs).then((res) => {
            swal("", "Thêm thành công!", "success");
            $("#modalAddSub_1").modal("hide");
            console.log("Create",this.props)
            this.props.onSuccess('Success');
        })
    }
//     shownewsub = (subs) =>{
//     var result = {};
//     result = subs.map((sub, index) =>{
//         return (
//             <li key={index} className="list-group-item left">
                 
//                 <button type="button" className="fas fa-times text-right del" onClick={()=>this.onDelSub(index)} style={{alignContent:"right"}}>
//                 </button>
//                 {sub.name}
//             </li>
            
//         )
//     })
//     return result;
// }
onDelSub = (index) => {
    var new_subs=[]
    var new_subs = this.state.subs;
    console.log("new_sub_state",new_subs);
    new_subs.splice(index,1)

    console.log("slice",new_subs);
    this.setState({
        subs:new_subs
    },()=>console.log(this.state.subs))
}

render() {
    var {subs} =this.state
    return (         
        <div>
             {/* Button trigger modal */}
                    {/* Modal */}
                    <div className="modal fade" id="modalAddSub_1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                    <div className="modal-content">

                        {/* Header */}
                        <div className="modal-header">
                        <h5 className="modal-title">Thêm Phân Loại </h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.onClose}>
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>

                        {/* Body */}
                        <div className="modal-body">
                        <form onSubmit={this.onSubmit}> 
                        <div className="form-group">
                            <label className="col-form-label">Tên Phân Loại:</label><br/>
                            <input type="text" name="name" className="form-control" onChange={this.onChange} value={subs.name}/>
                        </div>
                        </form>
                        </div>
                        <ul className="list-group">
                            {/* {this.shownewsub(subs)} */}
                        </ul>
                        
                        {/* Footer */}
                        <div className="modal-footer">
                        <button type="button" className="btn btn-danger" data-dismiss="modal"><i className="far fa-window-close"></i></button>
                        <button type="button" className="btn btn-info" onClick={this.onSave} id={this.state._id}><i className="fas fa-save"></i></button>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    )
}
}

