import React, {Component} from 'react'
import apiCaller from '../../../utils/apiCaller';
import $ from 'jquery'
import swal from 'sweetalert';

export default class ModalAddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subname:'',
            subs: 
                {name:"",
                id:''},
                subname1:""
        }
    }

    onChange = (e) =>{
        var target = e.target;
        var name = target.name;
        var value = target.value;	
        this.setState({
            [name] : value,
        })
    }
        onSubmit = (e) => {
        e.preventDefault()
        // var form={}
        // if(this.state.subname==="") {
        //     form = {
        //         name: this.props.name
        //     }
        // }
        //  else {
        //      form =  {
        //         name: this.state.subname
        //      } 
        // }
        apiCaller("categories/update/" + this.props.id_cate + "/sub_category/" + this.props.id_sub, "PUT", this.state.subname).then(res => {
            swal("Thành công", "Cập nhật phân loại sản phẩm thành công", "success")
            $('#modalEditSub').modal("hide")
        })
    }
    onSave = (event) =>{
        var subname = {
            name: this.state.subname
        }   
        apiCaller("categories/update/"+this.props.id_cate +"/sub_category/"+this.props.id_sub, "PUT", subname).then((res) => {
            swal("", "Cập nhật thành công!", "success");
            this.props.getList()
            $("#modalEditSub").modal("hide");
            console.log("Create",this.props)
            // this.props.onSuccess('Success');
        })
    }
onDelSub = (index) => {
    var new_subs=[]
    var new_subs = this.state.subs;
    console.log("new_sub_state",new_subs);
    new_subs.splice(index,1)

    console.log("slice",new_subs);
    this.setState({
        subs:new_subs
    },()=>console.log(this.state.subs))
}

render() {
    var {subname} =this.state
    var {name} = this.props
    return (         
        <div>
             {/* Button trigger modal */}
                    {/* Modal */}
                    <div className="modal fade" id="modalEditSub" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                    <div className="modal-content">

                        {/* Header */}
                        <div className="modal-header">
                        <h5 className="modal-title">Thông Tin Phân Loại </h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.onClose}>
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>

                        {/* Body */}
                        <div className="modal-body">
                        <form> 
                        <div className="form-group">
                            <label className="col-form-label">Tên Phân Loại:</label><br/>
                            <input type="text" name="subname" className="form-control" placeholder={name} onChange={(e) => this.onChange(e)} value={subname}/>
                            <button onClick={this.onSave} type="button" className="btn btn-info"> <i className="fas fa-save"></i></button>
                        </div>

                        </form>
                        </div>
                        {/* <ul className="list-group">
                            {this.shownewsub(subs)}
                        </ul>
                         */}
                        
                    </div>
                </div>
            </div>    
        </div>
    )
}
}

