import React, {Component} from 'react'
import apiCaller from '../../utils/apiCaller';
import $ from 'jquery'
import swal from 'sweetalert';
import { render } from "react-dom";

export default class ModalSubCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
           id_cate : '',
           sub_category : ''
        }
    }
    
    onClose = () => {this.setState
        ({}, () => $('#modalAddSub').modal("hide"))

    }
    onChange = (event) => {
        var {name,value} = event.target;
        this.setState({
            [name]:value
        })
    }
    handleAdd = () => {
        var {id_cate} = this.props;
        var header = {
            "Authorization": "Bearer " + JSON
                .parse(localStorage.getItem('user'))
                .token
        };
        var form = {
            name : this.state.sub_category
        }
        console.log("form",form);
         apiCaller("categories/create/"+id_cate+"/sub_category",'POST',form,header).then(res=>{
             console.log(res);
             swal("","Thêm thành công!", "success");
            
            //  this.props.success(res);
         })
      }
      componentWillReceiveProps(prop) {
          console.log(prop);
      }
    render() {
    
        var {id_cate} = this.props
        var {sub_category} = this.state
        return (
            <div>

                <div className="modal fade" id="modalAddSub">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center">
                                <h4 className="modal-title w-100 font-weight-bold"> DANH MỤC CON </h4>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>

                            <form>

                                <div className="form-group row">
                                    <div className='col-sm-1'></div>
                                    <label
                                        style={{
                                        fontWeight: 'lighter'
                                    }}
                                        className="col-sm-3 col-form-label">Tên phân loại</label>
                                    <div className="col-sm-8">
                                    
                                        <div className="md-form mt-0">
                                            <input 
                                             name="sub_category"
                                            value={sub_category}
                                             onChange={this.onChange}
                                             type="text" 
                                             className="form-control"
                                             ref="txtInput"/>
                                        </div>
                                    </div>
                                </div>

                                <div className="text-center">
                                    <button type="button" class="btn btn-primary btn-rounded blue-gradient"><i className="fas fa-plus"
                                    onClick={this.handleAdd}
                                    /></button>
                                    <button class="btn btn-primary btn-rounded blue-gradient"><i className="fas fa-edit"/></button>
                                    <button class="btn btn-primary btn-rounded blue-gradient"><i className="fas fa-trash"/></button>
                                </div>
                            </form>
                            

                            
                        </div>
                    </div>
                </div>
                <button
                    data-toggle="modal"
                    data-target="#modalAddSub"
                    className="btn btn-primary btn-md">THÊM PHÂN LOẠI</button>
            </div>
        )
    }
}
