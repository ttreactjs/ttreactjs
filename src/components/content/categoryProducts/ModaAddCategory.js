import React, {Component} from 'react'
import apiCaller from '../../../utils/apiCaller';
import $ from 'jquery'
import swal from 'sweetalert';

export default class ModalAddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            category: '',
            subs: [],
            name: '',
            item:''
        }
    }

    onChange = (e) =>{
        var target = e.target;
        var nam = target.name;
        var val = target.value;	
        this.setState({
            [nam] : val,
        })
    }
    onSubmit = (e) =>{
        e.preventDefault()
        e.target.reset()
        var {name} = this.state
        var text = {name};
        this.setState({
            name: '',
            subs: [...this.state.subs, text] 
        })
    }

    onSave = () =>{
        var {category, subs} = this.state
        apiCaller("categories/create/", "POST", {
            name: category,
            sub_category: subs
        }).then((res) => {
            swal("", "Thêm thành công!", "success");
            $("#ModalAddCate").modal("hide");
            this.props.getList()
        })
    }
    showModal = () =>{
        $("#ModalAddCate").modal("show");
    }
    shownewsub = (subs) =>{
    var result = {};
    result = subs.map((sub, index) =>{
        return (
            <li key={index} className="list-group-item left"> 
                <button type="button" className="fas fa-times text-right del" onClick={()=>this.onDelSub(index)} style={{alignContent:"right"}}>
                </button>
                {sub.name}
            </li>
        )
    })
    return result;
}
onDelSub = (index) => {
    var new_subs = this.state.subs;
    console.log("new_sub_state",new_subs);
    new_subs.splice(index,1)
    console.log("slice",new_subs);
    this.setState({
        subs:new_subs
    },()=>console.log(this.state.subs))
}

render() {
    var {subs} =this.state
    return (         
        <div>
             {/* Button trigger modal */}
             <button type="button" onClick={this.showModal} className="btn btn-primary mb-10 " data-toggle="modal" data-target="#ModalAddCate">THÊM LOẠI</button>
                    {/* Modal */}
                    <div className="modal fade" id="ModalAddCate" aria-labelledby="ModalAddCate" aria-hidden="true">
                    <div className="modal-dialog">
                    <div className="modal-content">

                        {/* Header */}
                        <div className="modal-header">
                        <h5 className="modal-title">Thêm Loại Sản Phẩm</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>

                        {/* Body */}
                        <div className="modal-body">
                        <div className="form-group">
                            <label className="col-form-label">Tên Loại:</label><br/>
                            <input type="text" name="category" className="form-control" onChange={this.onChange}/>
                        </div>
                        <form onSubmit={this.onSubmit}> 
                        <div className="form-group">
                            <label className="col-form-label">Tên Phân Loại:</label><br/>
                            <input type="text" name="name" className="form-control" onChange={this.onChange}/>
                        </div>
                        </form>
                        </div>
                        <ul className="list-group">
                            {this.shownewsub(subs)}
                        </ul>
                        
                        {/* Footer */}
                        <div className="modal-footer">
                        <button type="button" className="btn btn-danger" data-dismiss="modal"><i className="far fa-window-close"></i></button>
                        <button type="button" className="btn btn-info" onClick={this.onSave}><i className="fas fa-save"></i></button>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    )
}
}

