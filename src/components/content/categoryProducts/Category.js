import React, { Component } from 'react'
import apiCaller from '../../../utils/apiCaller'
import ReactTable from "react-table";
import "react-table/react-table.css";
import $ from 'jquery'
import { FormattedDate } from "react-intl";
import ModalAddCategory from './ModaAddCategory';
import ModalAddSub from './ModalAddSub';
import ModalEditSub from './ModalEditSub';
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom';
import '../../../App.css';
import "react-table/react-table.css";
import ReactPaginate from 'react-paginate';
import HeadingContent from '../HeadingContent';
export default class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      arr_sub:[],
      categories: [
        {id:'',
        name :''}
      ],
      id : '',
      id_cate : '',
      id_sub : '',
      name : '',
      form:
      {
        id:'',
        name:'',
        sub:''
      },
      crPage: 1,
      totalDocs: 0,
      docs:0,
      limit: 20,
      issuccess:false,
      name_sub_edit:''
    };
  }
  componentDidMount(){
   this.getList()
    };
  
  getList=()=>{
    var header = {
      "Authorization": "Bearer " + JSON
          .parse(localStorage.getItem('user'))
          .token
  };
  apiCaller("categories/list/manage?page=1&limit=20&status=","GET",null,header).then(res=>{
    console.log("cate",res);
    this.setState({categories:res.data.docs, totalDocs:res.data.totalDocs})
  });
};
  
  handleDeleteSub = (id_cate, id_sub) => {
    console.log("cate", id_cate, "sub", id_sub);
    var header = {
      "Authorization": "Bearer " + JSON
        .parse(localStorage.getItem('user'))
        .token
    };
    swal({
      text: "Bạn có muốn xóa phân loại sản phẩm này không?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
      .then((willDelete) => {
          if (willDelete) {
            var url = `categories/delete/${id_cate}/sub_category/${id_sub}`
            apiCaller(url, "DELETE", null, header).then(res => {
                  console.log(res);
              })
              swal("Bạn đã xóa thành công!", {
                  icon: "success",
              });
          } else {
          }
      });
  }
  handleAddSub = (id_cate) => {
    var header = {
      "Authorization": "Bearer " + JSON
        .parse(localStorage.getItem('user'))
        .token
    };
    apiCaller("categories/create/" + id_cate + "/sub_category", 'POST', this.state.name, header).then(res => {
      swal("", "Thêm thành công!", "success");
      console.log(res)
    })
  }

  displaySub = (data) => {
    console.log("display",data);
    var sub2 = data.sub_category.map((sub3, index) => {
      return <tr>
        <td>{sub3.name}</td>
        <td><button className="btn btn-primary btn-md" onClick={() => this.handleDeleteSub(data._id, sub3._id)}>Xóa</button>
        <button className="btn btn-primary btn-md" onClick={() => this.handleEditSub(data._id,data.sub_category[index]._id,data.sub_category[index].name)}>Sửa</button></td>
      </tr>
    });
    console.log(sub2);
    return sub2
  }
  handleEditSub=(id_cate,id_sub,name)=>{
    this.setState({
      id_cate:id_cate,
      id_sub:id_sub,
      name_sub_edit:name
    })
    $("#modalEditSub").modal("show");
  }

  onClickPage = (id) => {
    console.log(id);
    apiCaller("categories/list/manage?page=" + id + "&limit=20", "GET", null).then(data => {
      this.setState({ categories: data.data.docs })
      console.log(data);
    })
  }
  // Trạng thái
  activeOnClick = (id) => {
    var header = {
        "Authorization": "Bearer " + JSON
            .parse(localStorage.getItem('user'))
            .token
    };
    // Sweet alert
    swal({
        text: "Bạn có muốn kích hoạt loại sản phẩm này không?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                apiCaller("categories/active/" + id, "PUT", null, header).then(res => {
                    console.log(res);
                })
                swal("Bạn đã kích hoạt thành công!", {
                    icon: "success",
                });
            } else {
            }
        });
}
showModals =(event,id)=>{
  // console.log( $("#modalAddSub"));
  this.setState({
    id:id,
  },() =>{
    $("#modalAddSub_1").modal("show");
  }
  )
}
deactiveOnClick = (id) => {
    var header = {
        "Authorization": "Bearer " + JSON
            .parse(localStorage.getItem('user'))
            .token
    };
    swal({
        text: "Bạn có muốn vô hiệu hóa loại sản phẩm này không?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                apiCaller("categories/deactive/" + id, "PUT", null, header).then(res => {
                    console.log(res);
                })
                swal("Bạn đã vô hiệu hóa thành công!", {
                    icon: "success",
                });
            } else {}
        });
}
  // End trạng thái
  
  // Hành động của Category
  onSubmit = (event) => {
    event.preventDefault();
    var header = {
        "Authorization": "Bearer " + JSON
            .parse(localStorage.getItem('user'))
            .token
    };
    var form = {
        name: this.state.name,
        sub_category: this.state.arr_sub
    }
    apiCaller("categories/update/" + this.state.id, "PUT", form, header).then(data => {
      console.log("update",data);
  swal("", "Cập nhật thành công!", "success");
  this.getList()
})
}
  // End Hành động Category
editOnClick = async(event) => {
  var id = event.target.id
  await apiCaller("categories/details/" + id,"GET", null).then(data => {
      this.setState({
          id: id,
          name: data.data.name,
          arr_sub:data.data.sub_category
      }, () => {
          $("#modalEditCate").modal("show");
      })
  });
}
onSuccess = ( event)=>{
  this.getList()
}
 editOnChange = (event) => {
  const name = event.target.name;
  const value = event.target.value

  this.setState({  
    [name]: value
  }, () => console.log("Onchange: ", this.state.name))
}
onClose = () => {
  this.setState
      ({
      }, () => $('#modalEditCate').modal("hide"))
}
//Paginate
handlePageClick = (e) =>{
  var id = e.selected + 1;
  apiCaller("categories/list/manage?page="+id,"GET").then(data=>{
      console.log("page",data);
    this.setState({crPage:id,categories:data.data.docs})
    console.log(data);
  })
}
// End pagination
onDelSub = (index) => {
    var new_subs=[]
    var new_subs = this.state.arr_sub;
    new_subs.splice(index,1)
    this.setState({
        arr_sub:new_subs
    },()=>console.log(this.state.arr_sub))
}
updateAPI = async() => {
  var crPage = this.state.crPage
  await apiCaller("categories/list/manage?page=" + crPage, "GET", null).then(res => {
      console.log(res)
      this.setState({docs: res.data.docs})
  })
}

  render() {
    var {name} = this.state.form
    var { name,sub_cate } = this.state
    var { categories, redirect, totalDocs, limit } = this.state;
    var pagecount = Math.ceil(totalDocs / limit)
    if (redirect) return (<Redirect to="/loaisanpham" />)
   
    const columns = [
      {
        columns: [
          {
            Header: "STT",
            id: "row",
            maxWidth: 100,
            filterable: false,
            Cell: (row) => {
                return <div>{row.index+1}</div>;
            }
        },
          {
            Header: "Tên loại sản phẩm",
            accessor: "name"
          }
          
        ]
      },
      {
        columns: [
          {
            Header: "Ngày tạo",
            Cell : (row) => { return<span> <FormattedDate value={row.original.createdAt}
              day="2-digit" month="2-digit" year="numeric" hour="numeric" minute="numeric" /></span>
            }
          }
        ]
      },
      {
        columns: [
          {
            Header: "Trạng thái",
            id: "is_active",
            Cell: row => (
              <span>
                <span style={{
                  color: (row.original.is_active) ? '#57d500'
                    : '#ff2e00'
                }}>
                  &#x25cf;
                    </span> {
                  (row.original.is_active)
                    ? 'Kích hoạt'
                    : 'Vô hiệu hóa'
                }
              </span>
            )
          }
        ]
      },
      {
        columns: [
          {
            Header: "",
            maxWidth: 200,
            Cell: (row) => 
              <span>
                <i  onClick={this.editOnClick} id={row.original._id} className="fas fa-edit"></i>&nbsp;&nbsp;
                {(row.original.is_active == true)
                  ? <a className="fas fa-ban" onClick={() => this.deactiveOnClick(row.original._id)}></a>
                  : <i onClick={() => this.activeOnClick(row.original._id)} className="fas fa-check"></i>}
              </span>
            }
          ]
      }
    ];
   
    var arr_sub=[]
    var arr_sub = this.state.arr_sub;
    var show_sub;
    if(arr_sub.length > 0) {
        show_sub = arr_sub.map((sub,index)=>{
          return (<div>
              <button type="button" className="fas fa-times text-right del" onClick={()=>this.onDelSub(index)} style={{alignContent:"right"}}></button>
              <input
                value={sub.name}
                type="text"
                className="form-control "
         />
          </div>
         ) 
        })
    } 
    
    return (
      <div>
        <HeadingContent title="QUẢN LÝ SẢN PHẨM"/>
        <ModalAddCategory handlerSuccess ={this.onSuccess} getList={this.getList} className="mb-10"/>
        <ModalAddSub id_cate={this.state.id}  onSuccess={this.onSuccess} getList={this.getList}/>
        <ModalEditSub id_cate={this.state.id_cate} name={this.state.name_sub_edit} id_sub={this.state.id_sub} getList={this.getList}/>
        <ReactTable
          data={categories}
          columns={columns}
          showPagination={false}
          defaultPageSize={20}
          className="-striped -highlight"
          SubComponent={row => {
            console.log("row",row);
            return (
              <div>
                <button type="button" className="btn btn-primary mb-10 "  onClick={(e)=>this.showModals(e,row.original._id)}>THÊM PHÂN LOẠI</button>
                <table className="table table-striped table-bordered">
                  <thead style={{backgroundColor:"#3399FF"}}>
                    <th>Tên Phân Loại</th>
                    <th>Hành Động</th>
                  </thead>
                  <tbody>
                      {this.displaySub(row.original)}
                  </tbody>                 
                </table>
              </div>
            )}}
        />

  <div style={{align: 'center'}}>
    
              <ReactPaginate
              previousLabel={'Trở về'}
              disableInitialCallback={true}
              onPageChange={(i)=>this.handlePageClick(i)}
              nextLabel={'Tiếp theo'}
              pageCount={pagecount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              containerClassName={'pagination1'}
              pageClassName={'page-item'}
              pageLinkClassName={'page-link'}
              previousClassName={'page-link'}
              nextLinkClassName={'page-link'}
              activeClassName={"page-item active"}/>
  </div>
            <div className="modal fade" id="modalEditCate">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header text-center">
                                        <h4 className="modal-title w-100 font-weight-bold">CẬP NHẬT</h4>
                                        <button
                                            onClick={this.onClose}
                                            type="button"
                                            className="close"
                                            data-dismiss="modal"
                                            aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>

                                    <form onSubmit={this.onSubmit}>
                                        <div className="form-group row">
                                            <label className="col-sm-2 col-form-label"> Tên loại</label>
                                            <div className="col-sm-9">
                                                <div className="md-form mt-0">
                                                    <input
                                                        name="name"
                                                        onChange={this.editOnChange}
                                                        value={name}
                                                        type="text"
                                                        className="form-control "
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-sm-2 col-form-label"> Tên phân loại</label>
                                            <div className="col-sm-9">
                                                <div className="md-form mt-0">
                                                 {show_sub}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="modal-footer d-flex justify-content-center">
                                            <button type="submit" className="btn btn-indigo" onClick={this.onClose}>
                                                Xác nhận
                                                <i className="fas fa-paper-plane-o ml-1" />
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
          </div>
    )
  }
}
