import React, {Component} from 'react'
import apiCaller from '../../../utils/apiCaller';
import $ from 'jquery'
import swal from 'sweetalert';

export default class ModalAddCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subs:""
        }
    }
    
    onChange = (e) =>{
        var target = e.target;
        var name = target.name;
        var value = target.value;	
        this.setState({
            [name] : value,
        })
    }
    onSubmit = (e) =>{
        e.preventDefault()
        e.target.reset()
        var {name} = this.state
        var text = {name};
        // this.setState({
        //     name: '',
        //     subs: [...this.state.subs, text] 
        // })
    }
    onSave = () =>{
       var subs = {
           name: this.state.subs
       }
        apiCaller("categories/create/"+this.props.id_cate +"/sub_category", "POST", subs).then((res) => {
            swal("", "Thêm thành công!", "success");
            $("#modalAddSub_1").modal("hide");
            console.log("Create",res)
            this.props.onSuccess('Success');
            this.props.getList()
        })
    }
onDelSub = (index) => {
    var new_subs=[]
    var new_subs = this.state.subs;
    console.log("new_sub_state",new_subs);
    new_subs.splice(index,1)

    console.log("slice",new_subs);
    this.setState({
        subs:new_subs
    },()=>console.log(this.state.subs))
}

render() {
    var {subs,name} =this.state
    return (         
        <div>
             {/* Button trigger modal */}
                    {/* Modal */}
                    <div className="modal fade" id="modalAddSub_1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                    <div className="modal-content">

                        {/* Header */}
                        <div className="modal-header">
                        <h5 className="modal-title">Thêm Phân Loại </h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.onClose}>
                            <span aria-hidden="true">×</span>
                        </button>
                        </div>

                        {/* Body */}
                        <div className="modal-body">
                        <form> 
                        <div className="form-group">
                            <label className="col-form-label">Tên Phân Loại:</label><br/>
                            <input type="text" name="subs" className="form-control" onChange={this.onChange} value={name}/>
                        </div>
                        </form>
                        </div>
                       
                        {/* Footer */}
                        <div className="modal-footer">
                        <button type="button" className="btn btn-danger" data-dismiss="modal"><i className="far fa-window-close"></i></button>
                        <button type="button" className="btn btn-info" onClick={this.onSave} id={this.state._id}><i className="fas fa-save"></i></button>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    )
}
}

