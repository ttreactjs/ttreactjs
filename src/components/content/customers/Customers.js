import React, { Component } from 'react'
import apiCaller from '../../../utils/apiCaller'
import ReactPaginate from 'react-paginate';
import {Link} from 'react-router-dom'
import ReactTable from 'react-table'
import "react-table/react-table.css";
import HeadingContent from '../HeadingContent';
export default class ListCustomers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            docs: [],
            currentPage: 1,
            totalDocs: 0,
            limit: 10,
            active: ""
        };
    }
    componentDidMount() {
        console.log("didmount")
        apiCaller("payments/buyers?page=1&limit=10&sort=-createdAt&search=012345678", "GET", null).then(res => {
            this.setState({docs: res.data.docs, totalDocs: res.data.totalDocs})
        })
    }
//     onPageChange = (i) => {
//       var id = i.selected + 1
//       apiCaller("payments/buyers?page=1&limit=10&sort=-createdAt&search=012345678" + id, "GET",null).then(res => {
//           this.setState({docs: res.data.docs, currentPage: id})
//       })
//   }
  handlePageClick = (e) =>{
    var id = e.selected + 1;
    apiCaller("payments/buyers?page="+id,"GET").then(data=>{
        console.log("page",data);
      this.setState({crPage:id,docs:data.data.docs})
      console.log(data);
    })
  }
    render()
     {
      var {totalDocs, limit,docs} = this.state
      var pagecount = Math.ceil(totalDocs / limit);
        const columns = [
            {
              columns: [
                {
                  Header: "ID",
                  accessor: "_id"
              },
                {
                  Header: "Tên khách hàng",
                  id: "id",
                  accessor: 'name'
                },
                {
                  Header: "Email",
                  accessor: "email"
                },
                {
                  Header: "SDT",
                  accessor: "phone"
                },
                {
                  Header: "Địa Chỉ",
                  accessor: "address"
                },
                {
                  Header: "Mã Bưu Điện",
                  accessor: "postcode"
                }]},
                {
                  columns: [
                      {
                          width: 30,
                          Cell: row =><Link to={"/chitietkhachhang/"+row.original.id} className = "fas fa-info-circle"></Link>
                      }
                  ]
              }
           
        
    ];

        return (
            <div className="container-fluid">
      
                    <HeadingContent title="QUẢN LÝ KHÁCH HÀNG"/>

                    <div className="row wow fadeIn">
                        <div className="col-md-12 mb-4">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row"></div>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <div>
                                                <ReactTable
                                                    data={docs}
                                                    columns={columns}
                                                    defaultPageSize={10}
                                                    showPagination={false}
                                                    className="-striped -highlight"/>
                                            </div>

                                        </div>

                                    </div>
                                    
                                    <div
                                        style={{
                                        align: 'center'
                                    }}>

                                        <ReactPaginate
                                            previousLabel={'Trở về'}
                                            disableInitialCallback={true}
                                            onPageChange={(i)=>this.handlePageClick(i)}
                                            nextLabel={'Tiếp theo'}
                                            pageCount={pagecount}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            containerClassName={'pagination1'}
                                            pageClassName={'page-item'}
                                            pageLinkClassName={'page-link'}
                                            previousClassName={'page-link'}
                                            nextLinkClassName={'page-link'}
                                            activeClassName={"page-item active"}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        )
    }
}
