import React, {Component} from 'react'
import apiCaller from '../../../utils/apiCaller'
import '../../../App.css'
import  currencyFormatter from 'currency-formatter'
import { FormattedDate } from "react-intl";
export default class DetailCustomers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            docs: null,
            isShow:false
        };
    }

    componentDidMount() {
        var id = this.props.match.params.id
        apiCaller("payments/buyers/" + id, "GET").then(res => {
            this.setState({docs: res.data, orders: res.data.orders, isShow: true})
        })        
    }  
    render() {
        var {docs,orders,isShow} = this.state;
        var showOrder = []
        if (orders.length > 0) {
            showOrder = orders.map((order, index) => {
                // total_amount += order.amount;
                
                return (
                    <tr>
                        <td>
                            <strong>{index + 1}</strong>
                        </td>
                        <td>
                            <strong>{order._id}</strong>
                        </td>
                        <td>
                            <strong>{order.status}</strong>
                        </td>
                        <td>
                            {currencyFormatter.format(order.total_amount, { locale: 'vi-VN' })}
                        </td>
                        <td>
                        <strong>{order.description}</strong>
                        </td>
                        <td>
                        <strong><FormattedDate day="numeric" month="numeric" year="numeric" value={order.createdAt}/></strong>
                        </td>
                    </tr>
                )
            })
        }

        return (
           <div className="ml-5">
               {
                   isShow 
                   ? 
                   <div>
                    <h1 className="text text-center">
                     <span className="badge badge-primary"><strong>CHI TIẾT KHÁCH HÀNG</strong></span>
                    </h1>
                     <table className="table table-striped table-bordered text-center pf"
                                style={{
                                width: "80%"
                            }}>
                        <tbody>
                                <tr>
                                        <td colspan="2">
                                            <span className="text-monospace">Tên : &nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.name}
                                            </span>
                                        </td>
                                </tr>
                                <tr>
                                        <td>
                                            <span className="text-monospace">Email : &nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.email}
                                            </span>
                                        </td>
                                        <td>
                                            <span className="text-monospace">Số Điện Thoại : &nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.phone}
                                            </span>
                                        </td>
                                </tr>
                                <tr>
                                        <td>
                                            <span className="text-monospace">Địa Chỉ :&nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.address}
                                            </span>
                                        </td>
                                        <td> 
                                            <span className="text-monospace">Mã Bưu Điện :&nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.postcode}
                                            </span>
                                        </td>
                                </tr>
                                </tbody>                                 
                       
                 </table>
                 </div>
                 : ""

               }
               {
                   (isShow)
                   ?
                   <div className="text-center">
                       <h3>
                                <span className="badge badge-primary text-center">DANH SÁCH ĐƠN HÀNG</span>
                            </h3>
                            <br/>
                            <table
                                style={{
                                width: "65%"
                            }}
                                className="table table-bordered" style={{paddingleft:"50px"}}>
                                    <thead>
                                            <th className="font-weight-bold">STT</th>
                                            <th className="font-weight-bold">ID</th>
                                            <th className="font-weight-bold">TRẠNG THÁI</th>
                                            <th className="font-weight-bold">TỔNG TIỀN</th>
                                            <th className="font-weight-bold">MÔ TẢ</th>
                                            <th className="font-weight-bold">NGÀY TẠO</th>
                                    </thead>
                                <tbody>
                                    {showOrder}
                                </tbody>

                            </table>
               
                   </div>
                   :""
               }
               
               
            </div>
        )
    }
}

