import React, {Component} from 'react'

export default class HeadingContent extends Component {
    onLogout () {
        localStorage.clear();
        window.location.href = '/';
    }   
    render() {
        return (
            <div className="card mb-4 wow fadeIn">
                {/*Card content*/}
                <div className="card-body d-sm-flex justify-content-between">
                    <h4 className="mb-2 mb-sm-0 pt-1">
                        <a href="https://mdbootstrap.com/docs/jquery/">TRANG CHỦ</a>
                        <span>&nbsp;/&nbsp;</span>
                        <span>{this.props.title}</span>
                    </h4>
                    <button type="button" className="btn blue-gradient" onClick={this.onLogout}>Đăng Xuất</button>
                </div>
            </div>

        )
    }
}
