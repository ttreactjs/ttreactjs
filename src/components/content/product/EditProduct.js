import React, {Component} from 'react'
import $ from 'jquery'
import 'bootstrap'
import apiCaller from '../../../utils/apiCaller'
import swal from 'sweetalert';

export default class EditProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            price: "",
            quantity: "",
            desciption: "",
            discount: "",
            status: "",
            isload: false
        }
    }

    openModal = async() => {
        var {id} = this.props
        var data = await apiCaller("products/details/" + id, "GET", null)
        this.setState({
            title: data.data.title,
            price: data.data.price,
            quantity: data.data.quantity,
            desciption: data.data.desciption,
            discount: data.data.discount,
            status: data.data.status,

            isload: true
        }, () => $('#editproduct').modal("show"))

    }
    onChange = (e) => {
        var {name} = e.target;
        var value;
        (e.target.value === "")
            ? value = e.target.placeholder
            : value = e.target.value
        this.setState({[name]: value})

    }
    onSubmit = () => {
        console.log(this.state)
        apiCaller("products/update/" + this.props.id, "PUT", this.state).then(res => {
            swal("Thành công", "Cập nhật sản phẩm thành công", "success")
            this.props.updateAPI()
            $('#editproduct').modal("hide")
        })
    }
    closeModal = () => {
        $('#editproduct').modal("hide");
        this.setState({isload: false})
    }
    render() {
        var {
            title,
            price,
            quantity,
            desciption,
            discount,
            status
        } = this.state
        var {isload} = this.state
        return (
            <div>
                <i onClick={this.openModal} className="fas fa-edit"/> {isload
                    ? <div className="modal fade" id="editproduct">
                            <div className="row">
                            <div style={{marginLeft:"30%"}}  className="col-sm-8">
                                <div className="modal-dialog modal-lg">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h4 className="modal-title w-100">CẬP NHẬT SẢN PHẨM</h4>
                                            <button onClick={this.closeModal} type="button">
                                                <span>×</span>
                                            </button>
                                        </div>
                                        <div className="row">
                                            <form>

                                                <div className="form-group">
                                                    <label>TIÊU ĐỀ</label>
                                                    <input
                                                        onChange={(e) => this.onChange(e)}
                                                        name="title"
                                                        placeholder={title}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>

                                                <div className="form-group">
                                                    <label>MÔ TẢ</label>
                                                    <input
                                                        onChange={(e) => this.onChange(e)}
                                                        name="desciption"
                                                        placeholder={desciption}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>GIÁ</label>
                                                    <input
                                                        onChange={(e) => this.onChange(e)}
                                                        name="price"
                                                        placeholder={price}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                {/* Default input */}
                                                <div className="form-group">
                                                    <label>SỐ LƯỢNG</label>
                                                    <input
                                                        onChange={(e) => this.onChange(e)}
                                                        name="quantity"
                                                        placeholder={quantity}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>GIẢM GIÁ</label>
                                                    <input
                                                        onChange={(e) => this.onChange(e)}
                                                        name="discount"
                                                        placeholder={discount}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>TRẠNG THÁI</label>
                                                    {
                                                            status==='selling' 
                                                            ? 
                                                                <select name="status" onChange={this.onChange} className="form-control">
                                                                    <option selected value="selling">Đang bán</option>
                                                                    <option value="Pending">Đang chờ</option>
                                                                </select>
                                                            :
                                                                <select name="status" onChange={this.onChange} className="form-control">
                                                                    <option  value="selling">Đang bán</option>
                                                                    <option selected value="Pending">Đang chờ</option>
                                                                </select>
                                                    }
                                                        
                                                    
                                                </div>
                                            </form>
                                        </div>
                                        <div className="modal-footer">
                                            <button
                                                onClick={this.onSubmit}
                                                type="button"
                                                className="btn btn-primary btn-md">Xác nhận</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    </div>
                        </div>
                    : ""
}

            </div>
        )
    }
}
