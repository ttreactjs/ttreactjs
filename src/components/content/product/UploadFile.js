import React, {Component} from 'react'
import ImageUploader from 'react-images-upload';
import apiCaller from '../../../utils/apiCaller';
import swal from 'sweetalert'


export default class UploadFile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: [],
            isDisplay: true

        }
    }
    
    onClick = (e) => {
        e.preventDefault();
        this.setState({isDisplay: false})
        var file = e.target[1].files[0];
        this.onChangeFile(file)
        this.setState({inputKey: ""})

    }
    onChangeFile = async(file) => {
        var form = new FormData();
        var images = [];
        form.append('source', file);
        await apiCaller("upload/image", "POST", form).then(res => {
            if (!res) {
                swal("Thất bại!", {icon: "danger"});
            } else {
                images.push(res.data.url)
                this.props.updateImage(res.data.url);
                this.setState({
                    isDisplay: true,
                    images: images
                }, () => {
                    var addURL = {
                        images: images
                    }
                    apiCaller("products/" + this.props.id + "/images", "POST", addURL).then(res => {
                        swal("Thành công!", {icon: "success"});
                    })
                })

            }
        })

    }
    render() {
        var {isDisplay} = this.state
        return (
            <div>
                {isDisplay
                    ? <form onSubmit={this.onClick}>

                        <ImageUploader
                            key={this.state.inputKey}
                            id="images"
                            singleImage={true}
                            label="File ảnh tối đa 5MB"
                            withIcon={true}
                            onChange={this.onDrop}
                            buttonText='Hình sản phẩm'
                            imgExtension={['.jpg', '.gif', '.png', '.gif']}
                            maxFileSize={5242880}
                            withPreview={true}
                            style={{
                            width: "100%"
                        }}/>
                         <button style={{marginLeft:"70%"}} type="submit" className="btn-sm">Xác nhận Upload</button>
                    </form>
                    :<div>
                    <div className="spinner-grow text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <div className="spinner-grow text-secondary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <div className="spinner-grow text-success" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <div className="spinner-grow text-danger" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <div className="spinner-grow text-warning" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <div className="spinner-grow text-info" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <div className="spinner-grow text-light" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                    <div className="spinner-grow text-dark" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
                    }

            </div>

        )
    }
}
