import React, {Component} from 'react'
import apiCaller from '../../../utils/apiCaller'
import $ from 'jquery'
import 'bootstrap'
import swal from 'sweetalert';
import ImageUploader from 'react-images-upload';

export default class AddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            price: "",
            quantity: "",
            desciption: "",
            discount: "",
            status: "",
            is_active: true,
            images: [],
            isUpload: false,
            file: null
        }

    }
    //Start --Type content input
    onChangeInput = (e) => {
        const {name, value} = e.target;
        this.setState({[name]: value})

    }
    //End --Type content input Start --Open/Close Modal
    closeModal = () => {
        $('#modalAdd').modal("hide")
    }
    openModal = () => {
        $('#modalAdd').modal("show")
    }
    //End --Open/Close Modal Start --Change Image Input
    onChangeFile = (file) => {
        this.setState({file: file[0]})

    }

    uploadOnclick = async() => {
        var form = new FormData();
        var images = this.state.images;
        var file = this.state.file
        console.log(file);
        form.append('source', file);
        await apiCaller("upload/image", "POST", form).then(res => {
            if (!res) {
                swal("Thất bại!", {icon: "danger"});
            } else {
                images.push(res.data.url)
                this.setState({isDisplay: true, images: images})
                swal("Thành công!", "Đã thêm hình", "success")

            }
        })
    }
    //End --Change Image Input Start --Submit Form AddProduct
    onSubmit = (event) => {
        event.preventDefault();
        var header = {
            "Authorization": "Bearer " + JSON
                .parse(localStorage.getItem('user'))
                .token
        };
        apiCaller("products/create?", 'POST', this.state, header).then(res => {
            if (res) {
                swal("Thành công!", "Đã thêm sản phảm!", "success");
                this
                    .props
                    .updateAPI();
                this.closeModal();

            } else {
                swal("Thất bại!", "Có lỗi xảy ra!", "danger");
            }
        });

    }
    //End --Submit Form AddProduct

    render() {
        var {
            title,
            price,
            quantity,
            desciption,
            discount,
            images
        } = this.state;
        var image;
        if (images.length > 0) {
            image = images.map((img, index) => {
                return (<img key={index} alt={true} src={img} width='150px'/>)
            })
        }
        return (
            <div>
                <div className="modal fade" id="modalAdd">
                    <div className="row">
                        <div style={{marginLeft:"30%"}}  className="col-sm-8">
                            <div className="modal-dialog modal-lg">
                                <div className="modal-content">
                                    <div className="modal-header text-center">
                                        <h4 className="modal-title w-100 font-weight-bold">THÊM SẢN PHẨM MỚI</h4>
                                        <button onClick={this.closeModal} type="button" className="close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div>
                                        <div className="row">

                                            <form onSubmit={this.onSubmit}>

                                                <div className="form-group">
                                                    <label>TIÊU ĐỀ</label>
                                                    <input
                                                        name="title"
                                                        value={title}
                                                        onChange={this.onChangeInput}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>

                                                <div className="form-group">
                                                    <label>MÔ TẢ</label>
                                                    <input
                                                        name="desciption"
                                                        value={desciption}
                                                        onChange={this.onChangeInput}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>GIÁ</label>
                                                    <input
                                                        name="price"
                                                        value={price}
                                                        onChange={this.onChangeInput}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                {/* Default input */}
                                                <div className="form-group">
                                                    <label>SỐ LƯỢNG</label>
                                                    <input
                                                        name="quantity"
                                                        value={quantity}
                                                        onChange={this.onChangeInput}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>GIẢM GIÁ</label>
                                                    <input
                                                        name="discount"
                                                        value={discount}
                                                        onChange={this.onChangeInput}
                                                        type="text"
                                                        className="form-control"/>
                                                </div>
                                                <div className="form-group">
                                                    <label>TRẠNG THÁI</label>
                                                    <select className="form-control">
                                                        <option value="selling">Đang bán</option>
                                                        <option value="Pending">Đang chờ</option>

                                                    </select>
                                                </div>

                                                <div className="row">
                                                    {image}
                                                
                                                        <ImageUploader
                                                            id="images"
                                                            singleImage={true}
                                                            label="File ảnh tối đa 5MB"
                                                            withIcon={true}
                                                            onChange={this.onChangeFile}
                                                            buttonText='Hình sản phẩm'
                                                            imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                                            maxFileSize={5242880}
                                                            withPreview={true}
                                                            style={{
                                                            width: "100%"
                                                        }}/>
                                                        
                                                            <button style={{marginLeft:"70%"}} onClick={this.uploadOnclick} type="button" className="btn-sm">Xác nhận Upload</button>
                                                    
                                                    
                                                    <div className="row">
                                                        {(this.state.isUpload)
                                                            ? <div className="col-sm-2">
                                                                    <div className="spinner-border" role="status">
                                                                        <span className="sr-only">Vui lòng chờ</span>
                                                                    </div>

                                                                </div>
                                                            : ""}
                                                    </div>

                                                </div>

                                                <div className="modal-footer d-flex justify-content-center">
                                                    <button id="btn-one" type="submit" className="btn btn-indigo">Xác nhận
                                                    </button>
                                                </div>

                                            </form>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <button type="button" onClick ={this.openModal} className="btn btn-primary">Thêm sản phẩm</button>

            </div>
        )
    }
}
