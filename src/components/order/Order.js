import React, {Component} from 'react'
import apiCaller from '../../../utils/apiCaller'
import './style.css'
import ReactPaginate from 'react-paginate';
import {Link} from 'react-router-dom'
import ReactTable from 'react-table'
import "react-table/react-table.css";
import HeadingContent from '../HeadingContent';
import {FormattedDate, FormattedTime} from 'react-intl'
import swal from 'sweetalert'

export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            docs: [],
            currentPage: 1,
            totalDocs: 0,
            limit: 10,
            active: "",
            isSearch:false,
            search:""
        };
    }
    componentDidMount() {
        console.log("didmount")
        apiCaller("orders/list?page=1&limit=10", "GET", null).then(res => {
            if(res.data) {
                this.setState({docs: res.data.docs, totalDocs: res.data.totalDocs})
            }
           
        })

    }
    //Start ---Update new API
    updateAPI = async() => {
            var currentPage = this.state.currentPage     
            await apiCaller("orders/list?page=" + currentPage, "GET", null).then(res => { 
                console.log(res)         
                this.setState({docs: res.data.docs}) })
        }

    //End-- Toggle Active Start -- Change Page
    onPageChange = (i) => {
        var id = i.selected + 1
        apiCaller("orders/list?limti=10&page=" + id, "GET", null).then(res => {
            this.setState({docs: res.data.docs, currentPage: id})
        })

    }

    //End -- Change Page
    //Start-- Check Order
checkOrderStatus = (id) => {
    var obj = {
        status:"success"
    }
    swal({text: "Bạn có muốn duyệt đơn hàng này không?", icon: "warning", buttons: true, dangerMode: true}).then((willDelete) => {
            if (willDelete) {
                apiCaller("orders/check-status/" + id, "POST", obj).then(res => {
                    console.log(res);
                    if(!res){
                        swal("Đơn hàng đã được duyệt trước đó", {icon: "warning"});
                    }
                    else {
                        this.updateAPI();
                        swal("Duyệt sản phẩm thành công!", {icon: "success"});
                    }
                })

            } else {}
        });
        
    }
    //End-- Check Order

    render() {
        var {totalDocs, limit, docs,search} = this.state
        var pagecount = Math.ceil(totalDocs / limit);
        const columns = [
            {
                columns: [
                    {
                        Header: "ID",
                        accessor: "_id"
                    }
                ]
            }, {
                columns: [
                    {
                        Header: "KHÁCH HÀNG",
                        Cell: row => (row.original.buyer
                            ? row.original.buyer.name
                            : "Không có tên")
                    }
                ]
            }, {
                columns: [
                    {
                        Header: "MÔ TẢ",
                        accessor: "description"
                    }
                ]
            }, {
                columns: [
                    {
                        Header: "TỔNG HÓA ĐƠN",
                        accessor: "total_amount"
                    }
                ]
            }, {
                columns: [
                    {
                        Header: "TRẠNG THÁI",
                        Cell: row => (row.original.status === "SUCCESS"

                            ? <span onClick = {()=> this.checkOrderStatus(row.original._id)} className="badge badge-success">Thành công
                                </span>

                            : <span onClick = {()=> this.checkOrderStatus(row.original._id)} className="badge badge-danger">Đang chờ</span>)
                    }
                ]
            }, {
                columns: [
                    {
                        Header: "NGÀY MUA",
                        Cell: row => (
                            <div>
                                <FormattedDate
                                    value={row.original.createdAt}
                                    day="2-digit"
                                    month="2-digit"
                                    year="numeric"/>
                                ,&nbsp;
                                <FormattedTime value={row.original.createdAt}/>
                            </div>
                        )
                    }
                ]
            }, {
                columns: [
                    {
                        width: 30,
                        Cell: row =>< Link to = {
                            "/chitietdonhang/" + row.original.id
                        }
                        className = "fas fa-info-circle" > </Link>
                    }
                ]
            }
        ];
        return (

            <div className="container-fluid">

                <HeadingContent title="QUẢN LÝ HÓA ĐƠN"/>

                <div className="row wow fadeIn">
                    <div className="col-md-12 mb-4">
                        <div className="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div>
                                            <ReactTable
                                                data={docs}
                                                columns={columns}
                                                defaultPageSize={10}
                                                showPagination={false}
                                                className="-striped -highlight"/>
                                        </div>

                                    </div>

                                </div>
                                <div className="modal fade" id="imageModal">
                                    <div className="modal-dialog modal-lg" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h4 className="modal-title w-100" id="myModalLabel">Danh mục hình ảnh -
                                                </h4>
                                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div className="modal-body">

                                                <div>
                                                    <div className="spinner-grow text-primary" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-secondary" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-success" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-danger" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-warning" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-info" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-light" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    <div className="spinner-grow text-dark" role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    style={{
                                    align: 'center'
                                }}>

                                    <ReactPaginate
                                        previousLabel={'Trở về'}
                                        disableInitialCallback={true}
                                        onPageChange={(i) => this.onPageChange(i)}
                                        nextLabel={'Tiếp theo'}
                                        pageCount={pagecount}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        containerClassName={'pagination1'}
                                        pageClassName={'page-item'}
                                        pageLinkClassName={'page-link'}
                                        previousClassName={'page-link'}
                                        nextLinkClassName={'page-link'}
                                        activeClassName={"page-item active"}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
