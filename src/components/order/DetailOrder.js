import React, {Component, useRef} from 'react'
import apiCaller from '../../../utils/apiCaller'
import {FormattedDate, FormattedTime} from 'react-intl'
import currencyFormatter from 'currency-formatter'
import $ from 'jquery'
import 'bootstrap'
import ReactToPrint from 'react-to-print'

export default class DetailOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDisplay: false,
            docs: null,
            products: []
        }
    }

    componentDidMount() {
        var id = this.props.match.params.id
        console.log(this.props.history)
        apiCaller("orders/details/" + id, "GET").then(res => {
            this.setState({docs: res.data, products: res.data.products, isDisplay: true})
        })
    }

    render() {
        var {isDisplay, docs, products} = this.state;
        var showproduct = [];
        var total_amount = 0;
        if (products.length > 0) {
            showproduct = products.map((product, index) => {
                total_amount += product.amount;

                return (
                    <tr>
                        <td>
                            <strong>{index + 1}</strong>
                        </td>
                        <td>
                            <strong>{product.title}</strong>
                        </td>
                        <td >

                            {currencyFormatter.format(product.current_price, {locale: 'vi-VN'})}

                        </td>
                        <td>
                            <strong>{product.qty}</strong>
                        </td>
                        <td>
                            <strong>{product.current_discount}</strong>
                        </td>
                        <td>
                            {currencyFormatter.format(product.amount, {locale: 'vi-VN'})}
                        </td>
                    </tr>
                )
            })
        }

        return (

            <div className="ml-5">

                <h3 className="text text-center">
                    <span >CHI TIẾT HÓA ĐƠN</span>
                </h3>
                {isDisplay
                    ? <div >
                            <table
                                className="table table-bordered"
                                style={{
                                width: "30%"
                            }}>
                                <tbody>
                                    <tr>
                                        <td>
                                            <span className="text-monospace">ID: &nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs._id}
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span className="text-monospace">NGÀY TẠO: &nbsp;</span>
                                            <span className="text font-weight-bold text text-right"><FormattedDate
                                                value={docs.createdAt}
                                                day="2-digit"
                                                month="2-digit"
                                                year="numeric"/>
                                                ,&nbsp;
                                                <FormattedTime value={docs.createdAt}/>
                                            </span>
                                        </td>
                                    </tr>

                                </tbody>

                            </table>
                            <br></br>
                            <h3>
                                <span className="badge badge-primary">THÔNG TIN KHÁCH HÀNG</span>
                            </h3>
                            <br/>
                            <table
                                style={{
                                width: "70%"
                            }}
                                className="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <span className="text-monospace">KHÁCH HÀNG: &nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.buyer.name}
                                            </span>
                                        </td>
                                        <td>

                                            <span className="text-monospace">ĐỊA CHỈ: &nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.buyer.address}
                                            </span>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <span className="text-monospace">ĐIỆN THOẠI: &nbsp;</span>
                                            <span className="text font-weight-bold text text-right">{docs.buyer.phone}
                                            </span>
                                        </td>

                                        <td>
                                            <span className="text-monospace">EMAIL: &nbsp;</span>
                                            <span className="text font-weight-bold text-right">{docs.buyer.email}
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                            <br/>
                            <div
                                style={{
                                width: '60%'
                            }}>

                                <div className="row">
                                    <table className="table table-bordered">
                                        <thead>
                                            <th className="font-weight-bold">STT</th>
                                            <th className="font-weight-bold">SẢN PHẨM / MÔ TẢ</th>
                                            <th className="font-weight-bold">GIÁ</th>
                                            <th className="font-weight-bold">SỐ LƯỢNG</th>
                                            <th className="font-weight-bold">GIẢM GIÁ</th>
                                            <th className="font-weight-bold">THANH TOÁN</th>
                                        </thead>
                                        <tbody>
                                            {showproduct}
                                        </tbody>
                                    </table>

                                </div>

                            </div>

                            <div className="row">
                                <div
                                    style={{
                                    marginLeft: "50%"
                                }}
                                    className="md-form form-lg">
                                    <h3 type="text">
                                        <span className="font-weight-bold">TỔNG THANH TOÁN: {currencyFormatter.format(total_amount * 23000, {locale: 'vi-VN'})}
                                        </span>
                                    </h3>
                                </div>

                            </div>

                            
                        </div>
                    : ""
}   
            </div>
        )
    }
}
// export class Example extends React.Component {
//     render() {
//       return (
//         <div>
//           <ReactToPrint
//             trigger={() => <a href="#">Print this out!</a>}
//             content={() => this.componentRef}
//           />
//           <DetailOrder ref={el => (this.componentRef = el)} />
//         </div>
//       );
//     }
//   }