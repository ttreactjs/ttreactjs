import React, {Component} from 'react'

export default class Header extends Component {
    onLogout () {
        localStorage.clear();
        window.location.href = '/';
    }
    render() {
        return (

            <nav
                className="navbar fixed-top navbar-expand-md navbar-light white scrolling-navbar">
                <div className="container-fluid">
                 
                        <strong className="blue-text">LKDT</strong>
                  
                    <button type="button" className="btn blue-gradient" onClick={this.onLogout}>Đăng Xuất</button>
                </div>
            </nav>

        )
    }
}
