import React, { Component } from 'react'
import CustomerItems from './CustomerItems';
import {NavLink} from 'react-router-dom'
import apiCaller from '../../utils/apiCaller'

export default class ListCustomers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customers: [],
            currentPage: 1,
            totalDocs: 0,
            limit: 10,
            active: ""
        };
    }
    componentDidMount() {
        var header = {
            "Authorization": "Bearer " + JSON
                .parse(localStorage.getItem('user'))
                .token
        };
        apiCaller("payments/buyers?page=1&limit=10&sort=-createdAt&search=012345678", "GET", null, header).then(res => {
            console.log("customer",res);
            // this.setState({customers: res.data.docs, totalDocs: res.data.totalDocs})
        })
    }
    onClick = (event) => {
        const id = event.target.id;
        this.setState({currentPage: id})
        var header = {
            "Authorization": "Bearer " + JSON
                .parse(localStorage.getItem('user'))
                .token
        };

        apiCaller("payments/buyers?page="+id+"&limit=10&sort=-createdAt&search=012345678", "GET", null, header).then(res => {

            this.setState({customers: res.data.docs})
        })

    }
    render()
     {
        const columns = [
            {
              columns: [
                {
                  Header: "Tên khách hàng",
                  id: "id",
                  accessor: '_id'
                },
                {
                  Header: "Email",
                  accessor: "name"
                }
              ]
            },
            {
              columns: [
                {
                  Header: "Ngày tạo",
                  accessor: "createdAt"
                }
              ]
            },
            {
              columns: [
                {
                  Header: "Trạng thái",
                  id: "is_active",
                  Cell: row => (
                    <span>
                      <span style={{
                        color: (row.original.is_active) ? '#57d500'
                          : '#ff2e00'
                      }}>
                        &#x25cf;
                          </span> {
                        (row.original.is_active)
                          ? 'Kích hoạt'
                          : 'Vô hiệu hóa'
                      }
                    </span>
                  )
                }
              ]
            }
          ];

        var {customers, totalDocs, limit} = this.state;

        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(totalDocs / limit); i++) {
            pageNumbers.push(i);
        }

        var numPageBar = pageNumbers.map(index => {
            return (
                <li key={index} className="page-item">
                    <NavLink onClick={this.onClick} id={index} className="page-link">{index}</NavLink>
                </li>

            )
        })

        var listcustomer = customers.map((customer, index) => {
           
            return (<CustomerItems index={index} key={index} customer = {customer}/>)
        })
        return (
            <div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên khách hàng</th>
                            <th>Email</th>
                            <th>Điện thoại</th>
                            <th>Địa chỉ</th>
                            <th>Postcode</th>
                            <th>Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    {listcustomer}
                    </tbody>
                </table>
                <nav aria-label="Page navigation example">
                    <ul className="pagination pg-blue justify-content-center">

                        {numPageBar}
                    </ul>
                </nav>
            </div>
        )
    }
}
