import React, {Component} from 'react'
import ListOrdersofDetail from './ListOrdersofDetail';
import axios from 'axios';
export default class DetailCustomers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customers: {
                name : '', 
                id : '' 
                    }
        };
    }

    componentDidMount() {

        const url = "http://139.162.28.184:3456";
        axios({
            method: "GET",
            url: url + "/api/v1/payments/buyers"
        }).then(res => {
            this.setState({categories: res.data})
        })
    }
    render() {
        
        var {customers,index} = this.props
        return (
            <div>
                <h1 className="text text-center">
                    <strong className="blue-gradient">CHI TIẾT KHÁCH HÀNG</strong>
                </h1>
                
                <br/>
                <h3 className="text-center">
                    <span class="badge badge-default blue-gradient">THÔNG TIN CÁ NHÂN </span>
                </h3>
                <br/>
                <table className="text-center"
                    style={{
                    width: "90%",
                    margin : "0 auto"
                    
                }}
                    className="table table-bordered">
                    <tr>
                    <td>
                            <strong>ID KHÁCH HÀNG: 
                            </strong>
                        </td>
                        <td>
                            <strong>TÊN KHÁCH HÀNG :
                            </strong>
                        </td>
                        <td>
                            <strong>EMAIL:
                            </strong>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <strong>SỐ ĐIỆN THOẠI:
                            </strong>
                        </td>
                        <td>
                            <strong>ĐỊA CHỈ:
                            </strong>
                        </td>
                        <td>
                            <strong>MÃ BƯU ĐIỆN:
                            </strong>
                        </td>
                        
                    </tr>
                </table>
                <br/>
               
                <h3 className="text-center">
                    <span class="badge badge-default blue-gradient">DANH SÁCH HÓA ĐƠN MUA HÀNG </span>
                </h3>
                <table class="table table-hover"
                style={{
                    width: "90%",
                    margin : "0 auto"
                    
                }}
                >
                    <thead>
                        <tr>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID </td>
                            <td>Ngày tạo </td>
                            <td>Tổng hóa đơn </td>
                            <td>Trạng thái </td>
                            <td>Ngày mua </td>
                        </tr>
                    </tbody>
                    <div>
                        
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <ListOrdersofDetail/>
                            </tbody>
                        </table>
                        
                    </div>
                    
                </table>
                

               

            </div>
        )
    }
}
