import React, {Component} from 'react'
import {Link} from 'react-router-dom'
export default class CustomerItems extends Component {
    render() {
        var {customer,index} = this.props
        return (
            <tr>
                <td>{index+1}</td>
                <td>{customer.name}</td>
                <td>{customer.email}</td>
                <td>{customer.phone}</td>
                <td>{customer.address}</td>
                <td>{customer.postcode}</td>
                <td>
				<Link to="/chitietkhachhang" className="text-info">Xem chi tiết</Link>
				</td>
				
            </tr>
        )
    }
}
