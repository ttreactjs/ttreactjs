import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {IntlProvider} from 'react-intl';

ReactDOM.render(
    <IntlProvider locale="en">
        <App/>
    </IntlProvider>, document.getElementById('root'))

serviceWorker.unregister();
