import React, {Component} from 'react'
import Login from './components/login/Login';
import {BrowserRouter as Router} from 'react-router-dom'
import HomePage from './components/HomePage';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false
        }
    }

    checkLogin = (bool) => {
        if (localStorage.getItem('user') !== null) {
            this.setState({isLogin: true})
        }
        else {
            this.setState({isLogin: bool})
        }
    }
    componentDidMount() {
        if (localStorage.getItem('user') !== null) {
            this.setState({isLogin: true})
        }
    }
    

    render() {
        var {isLogin} = this.state
        return (
            <Router>
                    <div>
                        {
                            isLogin? <HomePage checkLogin= {this.checkLogin}/> : <Login checkLogin= {this.checkLogin}/>
                        }
                    </div>
            </Router>
        )
    }
}
