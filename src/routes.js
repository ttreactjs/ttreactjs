
// import ListProducts from "./components/products/ListProducts";
// import ListCategorys from "./components/categoryProducts/ListCategorys";

// import ListCustomers from "./components/customers/ListCustomers";
import Orders from "./components/content/order/Order"

// import DetailOrders from "./components/orders/DetailOrders";
// import SanPham from "./components/SanPham";
// import Login from "./components/login/Login";

// import Home from "./components/home/Home";
// import App from "./App";
// import HomePage from "./components/HomePage";
import Products from "./components/content/product/Products";
import React from 'react'
import DetailOrder from "./components/content/order/DetailOrder";
import Category from "./components/content/categoryProducts/Category";
import Customers from "./components/content/customers/Customers";
import DetailCustomers from "./components/content/customers/DetailCustomers";

const routes = [
 
    
    {
        path:'/sanpham',
        exact:false,
        main:({match}) => <Products match = {match}/>
    },
	{
        path:'/loaisanpham',
        exact:false,
        main: ({match}) => <Category match={match}/>
    },

    {
        path:'/khachhang',
        exact:false,
        main: ({match}) => <Customers match={match}/>
    },
	{
        path:'/chitietkhachhang/:id',
        exact:false,
        main: ({match}) => <DetailCustomers match={match}/>
    },
    {
        path:'/donhang',
        exact:false,
        main: ({match}) => <Orders match={match}/>
    },
    {
        path:'/chitietdonhang/:id',
        exact:false,
        main: ({match}) => <DetailOrder match={match}  />
    },
]
export default routes;